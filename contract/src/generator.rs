use rand::{Rng, SeedableRng};
use rand_chacha::ChaCha20Rng;

use crate::elgamal::ElGamal;
pub(crate) fn generate_randomness(players: Vec<&[u8]>) -> (Vec<u32>, Vec<Vec<u8>>) {
    let mut rng = ChaCha20Rng::from_entropy();
    let elgamal = ElGamal::new();
    let mut seeds = vec![0; players.len()];
    for i in 0..players.len() {
        seeds[i] = rng.gen_range(0..52);
    }

    let mut commitments = vec![vec![]; players.len()];
    for i in 0..players.len() {
        let c = seeds[i].to_string();
        let (c1, c2) = elgamal.encrypt(&c);
        let commitment = format!("{}|{}", c1, c2);
        for j in 0..players.len() {
            if i != j {
                commitments[j].push(commitment.clone().into_bytes());
            }
        }
    }

    for i in 0..players.len() {
        for j in 0..players.len() {
            if i != j {
                let commitment = String::from_utf8(commitments[i][j].clone()).unwrap();
                let parts: Vec<&str> = commitment.split('|').collect();
                let c1 = parts[0].parse::<u128>().unwrap();
                let c2 = parts[1].parse::<u128>().unwrap();
                let decrypted = elgamal.decrypt(c1, c2);
                if decrypted != seeds[i] {
                    panic!("Invalid commitment from player {}", i);
                }
            }
        }
    }

    let mut R = 0;
    let mut ciphertexts = vec![];
    for i in 0..players.len() {
        let commitment = String::from_utf8(commitments[i][0].clone()).unwrap();
        let parts: Vec<&str> = commitment.split('|').collect();
        let c1 = parts[0].parse::<u64>().unwrap();
        let c2 = parts[1].parse::<u64>().unwrap();
        ciphertexts.push((c1, c2));
        R += seeds[i];
    }
    R %= 52;

    let mut product_c1 = 1;
    let mut product_c2 = 1;
    for ciphertext in &ciphertexts {
        product_c1 = (product_c1 * ciphertext.0) % elgamal.prime;
        product_c2 = (product_c2 * ciphertext.1) % elgamal.prime;
    }

    (
        vec![R.try_into().unwrap()],
        vec![format!("{}|{}", product_c1, product_c2).into_bytes()],
    )
}
