use near_sdk::{
    borsh::{self, BorshDeserialize, BorshSerialize},
    env, near_bindgen,
    serde::{ser::SerializeStruct, Serialize, Serializer},
    AccountId,
};
use num::BigUint;
use std::collections::HashMap;

mod dkg;
mod elgamal;
mod generator;

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[derive(Debug, Clone, BorshDeserialize, BorshSerialize)]
pub struct Player {
    id: AccountId,
    public_key: Vec<u8>,
    private_key: Vec<u8>,
}

impl Serialize for Player {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state: <S as Serializer>::SerializeStruct =
            serializer.serialize_struct("Player", 2)?;
        state.serialize_field("id", &self.id)?;
        state.serialize_field("public_key", &self.public_key)?;
        state.serialize_field("private_key", &self.private_key)?;
        state.end()
    }
}

#[derive(Debug, Clone, BorshDeserialize, BorshSerialize)]
pub struct Game {
    id: u64,
    players: Vec<Player>,
    rng_seed: Option<u128>,
    elgamal: elgamal::ElGamal,
}

#[near_bindgen]
#[derive(Debug, Clone, BorshDeserialize, BorshSerialize)]
pub struct GameContract {
    games: Vec<Game>,
}

#[near_bindgen]
impl GameContract {
    pub fn create_game(&mut self, players: Vec<Player>) {
        let id = self.games.len() as u64;
        self.games.push(Game {
            id,
            players,
            rng_seed: None,
            elgamal: elgamal::ElGamal::new(),
        });
    }

    pub fn get_game(&self, game_id: u64) -> Game {
        self.games[game_id as usize].clone()
    }

    #[payable]
    pub fn join_game(&mut self, game_id: u64) {
        let game = &mut self.games[game_id as usize];
        let account_id = env::predecessor_account_id();
        let keys = dkg::generate_keys();
        let already_attending = game.players.iter().any(|x| x.id == account_id);
        if already_attending {
            return;
        }
        if let Some(key) = keys {
            game.players.push(Player {
                id: account_id.clone(),
                private_key: key.0,
                public_key: key.1,
            });
        }
        game.elgamal.add_participant(account_id.to_string());
    }

    #[payable]
    pub fn rng(&mut self, game_id: u64) {
        let game = &mut self.games[game_id as usize];
        let mut elgamal = game.elgamal.clone();

        if game.players.len() < 2 {
            env::log_str("Not enough players in the game.");
            return;
        }

        if game.rng_seed.is_some() {
            return;
        }
        let mut public_keys = HashMap::new();
        for account in &game.players {
            let pubs = u32::from_be_bytes([
                account.public_key[0],
                account.public_key[1],
                account.public_key[2],
                account.public_key[3],
            ]);
            public_keys.insert(pubs, BigUint::from_bytes_be(account.private_key.as_slice()));
        }

        let public_keys = dkg::dkg(
            &public_keys,
            &BigUint::from(elgamal.g),
            &BigUint::from(elgamal.p),
            &BigUint::from(elgamal.q),
        )
        .unwrap();
        let binding = public_keys.to_bytes_be();
        let chunks = binding.chunks(4).collect::<Vec<&[u8]>>();
        let randomness = generator::generate_randomness(chunks).0;

        elgamal.distribute_shares();

        let mut private_key_shares = Vec::new();
        for i in 0..game.players.len() {
            let share = elgamal.shares[i];
            let private_key_share = elgamal.compute_key_share(share);
            private_key_shares.push(private_key_share);
        }

        let private_key = elgamal.reconstruct_secret();
        let mut random: u128 = 0;
        for &val in randomness.iter().rev() {
            random <<= 32;
            random |= val as u128;
        }
        let seed = elgamal.decrypt(private_key, random);

        game.rng_seed = Some(seed);
    }
}
