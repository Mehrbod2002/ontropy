use num::{BigUint, Integer, ToPrimitive};
use polynomial;
use rand::rngs::OsRng;
use rand::{thread_rng, Rng, RngCore};
use ring::signature::{EcdsaKeyPair, KeyPair};
use std::collections::HashMap;

pub(crate) fn dkg(
    players: &HashMap<u32, BigUint>,
    g: &BigUint,
    p: &BigUint,
    q: &BigUint,
) -> Option<BigUint> {
    let n = players.len();
    let k = n - 1;

    let mut polys = HashMap::new();
    let mut commitments = HashMap::new();
    let mut shares = HashMap::new();
    let mut verification_values = HashMap::new();

    for (i, _) in players {
        let mut coeffs = Vec::new();
        coeffs.push(players[i].clone());
        for _ in 1..=k {
            coeffs.push(BigUint::from(
                thread_rng().gen_range(1..q.to_u32().unwrap()),
            ));
        }
        let poly = polynomial::Polynomial::new(coeffs);
        polys.insert(i.clone(), poly);
    }

    for (i, poly) in &polys {
        let mut bis = Vec::new();
        let mut fis = Vec::new();
        for j in 0..=k {
            let bj = g.modpow(&poly.data()[j], p);
            bis.push(bj);
            let fj = poly.eval(BigUint::from(j));
            fis.push(fj.mod_floor(q));
        }
        commitments.insert(i.clone(), bis);
        shares.insert(i.clone(), fis);
    }

    for (i, _) in &commitments {
        let mut verifications = Vec::new();
        for j in 0..=k {
            let mut product = BigUint::from(1u32);
            for (m, commitment_m) in &commitments {
                if m == i {
                    continue;
                }
                let jm = BigUint::from(j).modpow(&BigUint::from(m - i + k as u32 + 1), q);
                product *= commitment_m[j as usize].modpow(&jm, p);
                product %= p;
            }
            verifications.push(product);
        }
        for j in 0..=k {
            let fi = shares.get(i).unwrap()[j as usize].clone();
            let verification = g.modpow(&fi, p);
            if verification != verifications[j as usize] {
                return None;
            }
        }
        verification_values.insert(i.clone(), verifications);
    }

    let mut public_key = BigUint::from(1u32);
    for (_, si) in players {
        public_key *= si;
        public_key %= p;
    }

    Some(public_key)
}

pub(crate) fn generate_keys() -> Option<(Vec<u8>, Vec<u8>)> {
    let mut rng = OsRng;
    let algorithm = &ring::signature::ECDSA_P256_SHA256_FIXED_SIGNING;
    loop {
        let mut pkcs8 = vec![0u8; 512];
        rng.fill_bytes(pkcs8.as_mut());
        if let Ok(key_pair) = EcdsaKeyPair::from_pkcs8(algorithm, &pkcs8) {
            let public_key = key_pair.public_key().as_ref().to_vec();
            return Some((pkcs8, public_key));
        }
    }
}
