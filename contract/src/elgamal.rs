use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::{env, near_bindgen};
use primes::is_prime;
use rand::Rng;
use rug::Integer;
use std::ops::Sub;
use std::str::FromStr;

#[near_bindgen]
#[derive(Default, Debug, Clone, BorshDeserialize, BorshSerialize)]
pub struct ElGamal {
    pub p: u128,
    pub q: u128,
    pub g: u128,
    pub x: u128,
    pub y: u128,
    pub prime: u64,
    pub participants: Vec<String>,
    pub shares: Vec<u128>,
}

impl ElGamal {
    pub fn new() -> Self {
        let mut rng = rand::thread_rng();

        let p: Integer = Integer::from(rng.gen::<u128>()).next_prime();

        let mut q = Integer::from(rng.gen::<u128>()).next_prime();
        while Integer::from(&p).sub(Integer::from(1)) % &q != Integer::from(0) {
            q = q.next_prime();
        }

        let mut g = Integer::from(2);
        while g.clone().pow_mod(&q, &p) == Ok(Integer::from(1)) {
            g += Integer::from(1);
        }

        let x = Integer::from(rng.gen::<u128>()) % &q;

        let y = Integer::from(&g)
            .pow_mod(&Integer::from(&x), &p)
            .unwrap()
            .to_u128_wrapping();

        Self {
            p: p.to_u128_wrapping(),
            q: q.to_u128_wrapping(),
            g: g.to_u128_wrapping(),
            x: x.to_u128_wrapping(),
            y,
            prime: Self::generate_prime(),
            participants: vec![],
            shares: vec![],
        }
    }

    fn generate_prime() -> u64 {
        let mut p = rand::random::<u64>() % 10000 + 65536;
        while !is_prime(p) {
            p += 1;
        }
        p
    }

    pub fn encrypt(&self, m: &str) -> (String, String) {
        let mut rng = rand::thread_rng();
        let e = Integer::from(rng.gen_range(1..self.q));
        let a = Integer::from(self.g)
            .pow_mod(&e, &Integer::from(self.p))
            .unwrap();
        let b = Integer::from(self.y)
            .pow_mod(&e, &Integer::from(self.p))
            .unwrap()
            * Integer::from_str(m).unwrap()
            % Integer::from(self.p);
        (a.to_string(), b.to_string())
    }

    pub fn decrypt(&self, a: u128, b: u128) -> u128 {
        let m = b * Integer::from(a)
            .pow_mod(&Integer::from(self.q - self.x - 1), &Integer::from(self.p))
            .unwrap()
            % self.p;
        m.to_u128_wrapping()
    }

    pub fn add_participant(&mut self, account_id: String) {
        self.participants.push(account_id);
    }

    pub fn get_shares(&self) -> Vec<u128> {
        self.shares.clone()
    }

    pub fn distribute_shares(&mut self) {
        let mut rng = rand::thread_rng();
        let mut coefficients = Vec::with_capacity(self.participants.len());
        for _ in 0..self.participants.len() - 1 {
            coefficients.push(rng.gen_range(1..self.q));
        }

        let mut total_coefficient = coefficients.iter().fold(Integer::from(1), |acc, x| acc * x);
        total_coefficient %= self.q;

        for (i, participant) in self.participants.iter().enumerate() {
            let share = Integer::from(self.y)
                .pow_mod(&Integer::from(coefficients[i]), &Integer::from(self.p))
                .unwrap();
            self.shares.push(share.to_u128_wrapping());
            env::log_str(format!("{}'s share: {}", participant, share).as_str());
        }
        env::log_str(format!("Total Coefficient: {}", total_coefficient).as_str());
        self.shares.push(total_coefficient.to_u128_wrapping());
    }

    pub fn reconstruct_secret(&self) -> u128 {
        let mut numerator = Integer::from(1);
        let mut denominator = Integer::from(1);

        for i in 0..self.participants.len() {
            let xi = Integer::from(self.shares[i]);
            numerator *= &xi;
            for j in 0..self.participants.len() {
                if i != j {
                    let xj = Integer::from(self.shares[j]);
                    denominator *= &(&xi - xj);
                }
            }
        }
        let inv_denominator = denominator.invert(&self.q.into()).unwrap();
        let secret = numerator * inv_denominator;
        secret.to_u128_wrapping()
    }

    pub fn compute_key_share(&self, other_y: u128) -> u128 {
        let shared_secret = Integer::from(other_y)
            .pow_mod(&Integer::from(self.x), &Integer::from(self.p))
            .unwrap()
            .to_u128_wrapping();
        shared_secret
    }
}
